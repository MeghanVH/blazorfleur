#pragma checksum "C:\Users\Student\source\repos\FleurCRUD\FleurCRUD\Pages\Fleurs.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "0c867684aac25246cf71951192f404590d4860d4"
// <auto-generated/>
#pragma warning disable 1591
namespace FleurCRUD.Pages
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "C:\Users\Student\source\repos\FleurCRUD\FleurCRUD\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\Student\source\repos\FleurCRUD\FleurCRUD\_Imports.razor"
using System.Net.Http.Json;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Users\Student\source\repos\FleurCRUD\FleurCRUD\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\Users\Student\source\repos\FleurCRUD\FleurCRUD\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\Users\Student\source\repos\FleurCRUD\FleurCRUD\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\Users\Student\source\repos\FleurCRUD\FleurCRUD\_Imports.razor"
using Microsoft.AspNetCore.Components.WebAssembly.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\Users\Student\source\repos\FleurCRUD\FleurCRUD\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\Users\Student\source\repos\FleurCRUD\FleurCRUD\_Imports.razor"
using FleurCRUD;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\Users\Student\source\repos\FleurCRUD\FleurCRUD\_Imports.razor"
using FleurCRUD.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "C:\Users\Student\source\repos\FleurCRUD\FleurCRUD\_Imports.razor"
using FleurCRUD.Models;

#line default
#line hidden
#nullable disable
    [Microsoft.AspNetCore.Components.RouteAttribute("/fleurs")]
    public partial class Fleurs : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
            __builder.OpenElement(0, "h3");
            __builder.AddContent(1, "Fleurs ");
            __builder.OpenElement(2, "button");
            __builder.AddAttribute(3, "class", "btn btn-primary");
            __builder.AddAttribute(4, "style", "border-radius:50px");
            __builder.AddAttribute(5, "onclick", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#nullable restore
#line 8 "C:\Users\Student\source\repos\FleurCRUD\FleurCRUD\Pages\Fleurs.razor"
                                                                                (() => RedirectAdd())

#line default
#line hidden
#nullable disable
            ));
            __builder.AddContent(6, "+");
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.AddMarkupContent(7, "\r\n");
            __builder.OpenElement(8, "ul");
            __builder.AddMarkupContent(9, "\r\n");
#nullable restore
#line 10 "C:\Users\Student\source\repos\FleurCRUD\FleurCRUD\Pages\Fleurs.razor"
     foreach (var fleur in ListeFleurs)
    {

#line default
#line hidden
#nullable disable
            __builder.AddContent(10, "        ");
            __builder.OpenElement(11, "li");
            __builder.AddMarkupContent(12, "\r\n            ");
            __builder.OpenElement(13, "p");
            __builder.AddMarkupContent(14, "\r\n                ");
            __builder.AddContent(15, 
#nullable restore
#line 14 "C:\Users\Student\source\repos\FleurCRUD\FleurCRUD\Pages\Fleurs.razor"
                 fleur.Name_fr

#line default
#line hidden
#nullable disable
            );
            __builder.AddMarkupContent(16, "\r\n                ");
            __builder.OpenElement(17, "button");
            __builder.AddAttribute(18, "class", "btn btn-info");
            __builder.AddAttribute(19, "onclick", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#nullable restore
#line 16 "C:\Users\Student\source\repos\FleurCRUD\FleurCRUD\Pages\Fleurs.razor"
                                                       (() => RedirectUpdate(fleur.Id))

#line default
#line hidden
#nullable disable
            ));
            __builder.AddMarkupContent(20, "<i class=\"fas fa-edit\"></i>");
            __builder.CloseElement();
            __builder.AddMarkupContent(21, "\r\n                ");
            __builder.OpenElement(22, "button");
            __builder.AddAttribute(23, "class", 
#nullable restore
#line 17 "C:\Users\Student\source\repos\FleurCRUD\FleurCRUD\Pages\Fleurs.razor"
                                 fleur.IsDeleted ? "btn btn-success" : "btn btn-danger"

#line default
#line hidden
#nullable disable
            );
            __builder.AddAttribute(24, "onclick", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#nullable restore
#line 17 "C:\Users\Student\source\repos\FleurCRUD\FleurCRUD\Pages\Fleurs.razor"
                                                                                                    (() => SoftDelete(fleur.Id))

#line default
#line hidden
#nullable disable
            ));
            __builder.OpenElement(25, "i");
            __builder.AddAttribute(26, "class", 
#nullable restore
#line 17 "C:\Users\Student\source\repos\FleurCRUD\FleurCRUD\Pages\Fleurs.razor"
                                                                                                                                              fleur.IsDeleted ? "fas fa-history" : "far fa-trash-alt"

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.AddMarkupContent(27, "\r\n            ");
            __builder.CloseElement();
            __builder.AddMarkupContent(28, "\r\n            \r\n        ");
            __builder.CloseElement();
            __builder.AddMarkupContent(29, "\r\n");
#nullable restore
#line 21 "C:\Users\Student\source\repos\FleurCRUD\FleurCRUD\Pages\Fleurs.razor"

    }

#line default
#line hidden
#nullable disable
            __builder.CloseElement();
        }
        #pragma warning restore 1998
#nullable restore
#line 26 "C:\Users\Student\source\repos\FleurCRUD\FleurCRUD\Pages\Fleurs.razor"
       

    List<pointInteret> ListeFleurs = new List<pointInteret>();

    protected override async Task OnInitializedAsync()
    {
        try
        {
            var res = await client.GetAsync("pointinteret");
            if (res.IsSuccessStatusCode)
            {
                string json = await res.Content.ReadAsStringAsync();
                ListeFleurs = Newtonsoft.Json.JsonConvert.DeserializeObject<List<pointInteret>>(json);
                ListeFleurs = ListeFleurs.Where(x => x.Category_id == 2).ToList();
            }

        }
        catch (Exception e)
        {
            throw;
        }
    }

    private void RedirectAdd()
    {
        NavManager.NavigateTo("/addFlower");
    }
    private void RedirectUpdate(int id)
    {
        NavManager.NavigateTo("/update/"+ id);
    }

    // Hard Delete
    //private Task Delete(int id)
    //{
    //    pointInteret lafleuradel = ListeFleurs.FirstOrDefault(x => x.Id == id);
    //    ListeFleurs.Remove(lafleuradel);
    //    return client.DeleteAsync("pointinteret/" + id);
    //}

    private Task SoftDelete(int id)
    {
        pointInteret lafleuradel = ListeFleurs.FirstOrDefault(x => x.Id == id);
        lafleuradel.IsDeleted = !lafleuradel.IsDeleted;
        return client.PutAsJsonAsync("pointinteret" , lafleuradel);

    }

#line default
#line hidden
#nullable disable
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private NavigationManager NavManager { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private HttpClient client { get; set; }
    }
}
#pragma warning restore 1591
